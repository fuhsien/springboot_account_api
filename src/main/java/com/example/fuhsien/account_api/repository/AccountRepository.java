package com.example.fuhsien.account_api.repository;

import java.util.List;
import java.util.Optional;

import com.example.fuhsien.account_api.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;




public interface AccountRepository extends JpaRepository<Account, Long> {
        Optional<Account> findByContactId(long contact_id);
}
