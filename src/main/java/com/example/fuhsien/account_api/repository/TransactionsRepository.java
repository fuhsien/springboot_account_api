package com.example.fuhsien.account_api.repository;

import com.example.fuhsien.account_api.model.Account;
import com.example.fuhsien.account_api.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface TransactionsRepository extends JpaRepository<Transaction, Long> {
        List<Transaction> findByAccountId(long ac_id);
}
