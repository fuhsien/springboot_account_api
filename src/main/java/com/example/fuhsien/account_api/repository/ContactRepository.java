package com.example.fuhsien.account_api.repository;

import com.example.fuhsien.account_api.model.Account;
import com.example.fuhsien.account_api.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface ContactRepository extends JpaRepository<Contact, Long> {
        Optional<Contact> findByContactTitle(String name);

        Optional<Contact> findByContactCIF(String identity_no);

        List<Contact> findByContactPhoneNo(String phone);

        List<Contact> findByContactEmail(String email);
}
