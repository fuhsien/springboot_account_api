package com.example.fuhsien.account_api.controller;

import com.example.fuhsien.account_api.model.Account;
import com.example.fuhsien.account_api.model.Contact;
import com.example.fuhsien.account_api.model.Transaction;
import com.example.fuhsien.account_api.repository.AccountRepository;
import com.example.fuhsien.account_api.repository.ContactRepository;
import com.example.fuhsien.account_api.repository.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class MainController {

    @Autowired
    AccountRepository accRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    TransactionsRepository txnRepository;

    /*
    * Mapping for Accounts
    * */
    @GetMapping("/accounts/all")
    public ResponseEntity<List<Account>> getAllAccounts(){
        try {
            List<Account> accounts = new ArrayList<Account>();
            accRepository.findAll().forEach(accounts::add);

            if (accounts.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(accounts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/accounts/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable("id") long id){
        try {
            Optional<Account> accountData = accRepository.findById(id);

            if (accountData.isPresent()){
                return new ResponseEntity<>(accountData.get(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping("/accounts")
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        try {
            // Check if provided contact ID is available
            long contact_id = account.getContactId();
            Optional<Contact> existContact = contactRepository.findById(contact_id);

            if (existContact.isEmpty()) {
                return new ResponseEntity("Contact ID: " + contact_id + " not exist. Please create contact first.", HttpStatus.FORBIDDEN);
            }

            Account _account = accRepository.save(new Account(account.getContactId(), account.getAccountBalance(), LocalDateTime.now(), null));
            return new ResponseEntity<>(_account, HttpStatus.CREATED);


        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     * Mapping for Contacts
     * */

    @GetMapping("/contacts/all")
    public ResponseEntity<List<Contact>> getAllContact(){
        try {
            List<Contact> contacts = new ArrayList<Contact>();
            contactRepository.findAll().forEach(contacts::add);

            if (contacts.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(contacts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/contacts/{id}")
    public ResponseEntity<Contact> getContactById(@PathVariable("id") long id){
        try {
            Optional<Contact> contactData = contactRepository.findById(id);

            if (contactData.isPresent()){
                return new ResponseEntity<>(contactData.get(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @PostMapping("/contacts")
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
        try {
            //need to check for repeated name
            Optional<Contact> existName = contactRepository.findByContactTitle(contact.getContactTitle().toUpperCase());
            Optional<Contact> existCIF = contactRepository.findByContactCIF(contact.getContactCIF());

            if (existName.isPresent()){
                return new ResponseEntity("Name: "+contact.getContactTitle().toUpperCase()+" already exist!", HttpStatus.FORBIDDEN);
            } else if (existCIF.isPresent()){
                return new ResponseEntity("Contact Identity No: "+contact.getContactCIF()+" already exist!", HttpStatus.FORBIDDEN);
            }


            Contact _contact = contactRepository.save(new Contact(contact.getContactTitle().toUpperCase(), contact.getContactCIF(), contact.getContactPhoneNo(), contact.getContactEmail(), LocalDateTime.now(), null));
            return new ResponseEntity<>(_contact, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/contacts/update")
    public ResponseEntity<Contact> updateContact(@RequestBody Contact contact) {
        try {
            String newTitle = contact.getContactTitle().toUpperCase();
            String newCIF = contact.getContactCIF();
            String newPhone = contact.getContactPhoneNo();
            String newEmail = contact.getContactEmail();

            Optional<Contact> existContact = contactRepository.findById(contact.getId());
            Optional<Contact> existName = contactRepository.findByContactTitle(contact.getContactTitle().toUpperCase());
            Optional<Contact> existCIF = contactRepository.findByContactCIF(contact.getContactCIF());


            boolean badData = false;
            String errorMsg = "";

            //Validation
            if (existContact.isEmpty()){
                badData = true;
                errorMsg = "Contact ID: "+contact.getId()+" does not exist!";
            } else if (newTitle.isEmpty()){
                badData = true;
                errorMsg = "Contact Title(Name) cannot be empty";
            } else if (newCIF.isEmpty()){
                badData = true;
                errorMsg = "Contact CIF(IC) cannot be empty";
            } else if (existName.isPresent() && !newTitle.equals(existContact.get().getContactTitle())){
                badData = true;
            } else if (existCIF.isPresent() && !newCIF.equals(existContact.get().getContactCIF())){
                badData = true;
                errorMsg = "Contact Identity No: "+ newCIF +" already exist!";
            }

            if (badData) {
                return new ResponseEntity(errorMsg, HttpStatus.FORBIDDEN);
            }

            Contact _contact = existContact.get();
            _contact.setContactTitle(newTitle);
            _contact.setContactCIF(newCIF);
            _contact.setContactPhoneNo(newPhone);
            _contact.setContactEmail(newEmail);
            _contact.setContactUpdated();

            return new ResponseEntity<>(contactRepository.save(_contact), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     * Mapping for Transactions
     * */
    @GetMapping("/transactions/all")
    public ResponseEntity<List<Transaction>> getAllTransactions(){
        try {
            List<Transaction> transactions = new ArrayList<Transaction>();
            txnRepository.findAll().forEach(transactions::add);

            if (transactions.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(transactions, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping("/transactions/history/{account_id}")
    public ResponseEntity<List<Transaction>> getAccountTransactionHistory(@PathVariable("account_id") long accountId){
        try {
            List<Transaction> accTransactions = new ArrayList<Transaction>();
            txnRepository.findByAccountId(accountId).forEach(accTransactions::add);
            
            List<Transaction> _accTransactions = new ArrayList<Transaction>();
            for (Transaction currTransaction : accTransactions) {
                Duration duration = Duration.between(currTransaction.getTransactionDate(), LocalDateTime.now());

                if (duration.toDays() <= 30) {
                    _accTransactions.add(currTransaction);
                }
            }

            if (_accTransactions.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(_accTransactions, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @PostMapping("/transactions")
    @Transactional
    public ResponseEntity<Transaction> createTransactions(@RequestBody Transaction transaction) {
        try {
            /*
            * Variable Definition
            * */
            ArrayList<String> validType = new ArrayList<String>();
            validType.add("WITHDRAW");
            validType.add("DEPOSIT");

            String type = transaction.getTransactionType().toUpperCase();
            long accountId = transaction.getAccountId();
            BigDecimal amount = transaction.getTransactionAmount();
            LocalDateTime txnDate = LocalDateTime.now();
            String status = "OK";
            String remarks = "";
            Optional<Account> account  = accRepository.findById(accountId);
            /*
             * Validation
             * */
            boolean badData = false;
            String errorMsg = "";

            if ( !validType.contains(type) ) {
                badData = true;
                errorMsg = "Invalid Transaction type: "+ type +". Use only: "+String.join("/ ",validType);
            } else if (account.isEmpty()) {
                badData = true;
                errorMsg = "Cannot find Account: "+ accountId ;
            }

            if (badData){
                return new ResponseEntity(errorMsg, HttpStatus.FORBIDDEN);
            }


            /*
             * Processing
             * */
            BigDecimal currBalance = account.get().getAccountBalance();
            BigDecimal newBalance;

            /*
            * Scenario 1: deposit money
            * Scenario 2: withdraw, sufficient fund
            * Scenario 3: withdraw, insufficient fund
            * */
            if (type.equals("WITHDRAW") && currBalance.compareTo(amount) < 0 ){
                status="FAIL";
                remarks = "Insufficient Fund. Balance Available: "+ currBalance;
            } else if (type.equals("DEPOSIT") || type.equals("WITHDRAW")){
                if (type.equals("WITHDRAW"))  {
                    newBalance = currBalance.subtract(amount);
                } else {    //deposit
                    newBalance = currBalance.add(amount);
                }

                // update info in account table
                Account _account = account.get();
                _account.setAccountBalance(newBalance);
                _account.setAccountLastTransactionDate(txnDate);
                accRepository.save(_account);   //consider check status
            }

            //save transaction
            Transaction _transaction = txnRepository.save(new Transaction(
                    accountId,
                    status,
                    type,
                    amount,
                    txnDate,
                    remarks)
            );
            return new ResponseEntity<>(_transaction, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
