package com.example.fuhsien.account_api.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "Transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_generator")
    @SequenceGenerator(name="transaction_generator", sequenceName = "tx_id_seq")
    private long tx_id;

    @Column(name = "tx_ac_id")
    private long accountId;

    @Column(name = "tx_status")
    private String transactionStatus;

    @Column(name = "tx_type")
    private String transactionType;

    @Column(name = "tx_amount", precision = 10, scale=2)
    private BigDecimal transactionAmount;

    @Column(name = "tx_date")
    private LocalDateTime transactionDate;

    @Column(name = "tx_remark")
    private String transactionRemark;

    public Transaction(){}

    public Transaction(long acc_id, String status, String type, BigDecimal amount, LocalDateTime tx_date, String remarks) {
        this.accountId = acc_id;
        this.transactionStatus = status;
        this.transactionType = type;
        this.transactionAmount = amount;
        this.transactionDate = tx_date;
        this.transactionRemark = remarks;
    }

    public long getID(){
        return tx_id;
    }

    public long getAccountId(){
        return accountId;
    }

    public void setAccountId(long acc_id){
        this.accountId = acc_id;
    }

    public String getTransactionStatus(){
        return transactionStatus;
    }

    public void setTransactionStatus(String status){
        this.transactionStatus = status;
    }

    public String getTransactionType(){
        return transactionType;
    }

    public void setTransactionType(String tx_type){
        this.transactionType = tx_type;
    }

    public BigDecimal getTransactionAmount(){
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal amount){
        this.transactionAmount = amount;
    }

    public LocalDateTime getTransactionDate(){
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime creation_date){
        this.transactionDate = creation_date;
    }

    public void setTransactionDate(){
        this.transactionDate = LocalDateTime.now();
    }

    public String getTransactionRemark(){
        return transactionRemark;
    }

    public void getTransactionRemark(String remark){
        this.transactionRemark = remark;
    }

    @Override
    public String toString() {
        return "Transaction [id=" + tx_id + ", AccountId=" + accountId + ", Status" + transactionStatus + ", Type=" + transactionType + ", Amount" + transactionAmount + ", Date=" + transactionDate + "]";
    }

}
