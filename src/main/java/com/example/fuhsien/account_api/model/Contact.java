package com.example.fuhsien.account_api.model;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Contacts")
public class Contact {

    @Id
    @Column(name = "ct_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_generator")
    @SequenceGenerator(name="contact_generator", sequenceName = "ct_id_seq")
    private long id;

    @Column(name = "ct_title")
    private String contactTitle;

    @Column(name = "ct_cif")
    private String contactCIF;

    @Column(name = "ct_phoneno")
    private String contactPhoneNo;

    @Column(name = "ct_email")
    private String contactEmail;

    @Column(name = "ct_created")
    private LocalDateTime contactCreated;

    @Column(name = "ct_updated")
    private LocalDateTime contactUpdated;

    public Contact(){}

    public Contact(String title, String identity_no, String phone, String email, LocalDateTime created, LocalDateTime updated) {
        this.contactTitle = title;
        this.contactCIF = identity_no;
        this.contactPhoneNo = phone;
        this.contactEmail = email;
        this.contactCreated = created;
        this.contactUpdated = updated;
    }

    public long getId(){
        return id;
    }

    public String getContactTitle(){
        return contactTitle;
    }

    public void setContactTitle(String name){
        this.contactTitle = name;
    }

    public String getContactCIF(){
        return contactCIF;
    }

    public void setContactCIF(String identity_no){
        this.contactCIF = identity_no;
    }

    public String getContactPhoneNo(){
        return contactPhoneNo;
    }

    public void setContactPhoneNo(String phone_no){
        this.contactPhoneNo = phone_no;
    }

    public String getContactEmail(){
        return contactEmail;
    }

    public void setContactEmail(String email){
        this.contactEmail = email;
    }

    public LocalDateTime getContactCreated(){
        return contactCreated;
    }

    public void setContactCreated(LocalDateTime creation_date){
        this.contactCreated = creation_date;
    }

    public void setContactCreated(){
        this.contactCreated = LocalDateTime.now();
    }

    public LocalDateTime getContactUpdated(){
        return contactUpdated;
    }

    public void setContactUpdated(LocalDateTime update_date){
        this.contactUpdated = update_date;
    }

    public void setContactUpdated(){
        this.contactUpdated = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Contact [contactID=" + id + ", Name=" + contactTitle + ", Contact CIF=" + contactCIF + ", Phone=" + contactPhoneNo + ", Email" + contactEmail + ", Created=" + contactCreated + ", Updated=" + contactUpdated + "]";
    }

}
