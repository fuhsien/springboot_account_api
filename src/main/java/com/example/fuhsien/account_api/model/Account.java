package com.example.fuhsien.account_api.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Entity
@Table(name = "Accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_generator")
    @SequenceGenerator(name="account_generator", sequenceName = "ac_id_seq")
    private long ac_id;

    @Column(name = "ac_contact_id")
    private long contactId;

    @Column(name = "ac_balance", precision = 10, scale=2)
    private BigDecimal accountBalance;

    @Column(name = "ac_created")
    private LocalDateTime accountCreated;

    @Column(name = "ac_last_txn_date")
    private LocalDateTime accountLastTransactionDate;

    public Account(){}

    public Account(long ct_id, BigDecimal balance, LocalDateTime created, LocalDateTime lasttxn) {
        this.contactId = ct_id;
        this.accountBalance = balance;
        this.accountCreated = created;
        this.accountLastTransactionDate = lasttxn;
    }

    public long getID(){
        return ac_id;
    }

    public long getContactId(){
        return contactId;
    }

    public void setContactId(long ct_id){
        this.contactId = ct_id;
    }

    public BigDecimal getAccountBalance(){
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal acc_bal){
        this.accountBalance = acc_bal;
    }

    public LocalDateTime getAccountCreated(){
        return accountCreated;
    }

    public void setAccountCreated(LocalDateTime creation_date){
        this.accountCreated = creation_date;
    }

    public void setAccountCreated(){
        this.accountCreated = LocalDateTime.now();
    }

    public LocalDateTime getAccountLastTransactionDate(){
        return accountLastTransactionDate;
    }

    public void setAccountLastTransactionDate(LocalDateTime txn_date){
        this.accountLastTransactionDate = txn_date;
    }

    @Override
    public String toString() {
        return "Account [Account ID=" + ac_id + ", Contact ID=" + contactId + ", Account Balance=" + accountBalance + ", Account Created=" + accountCreated + ", Last Transaction=" + accountLastTransactionDate + "]";
    }

}
